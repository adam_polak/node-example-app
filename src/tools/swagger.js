const jsdoc = require('swagger-jsdoc');
const packageJson = require('../../package');

module.exports = jsdoc({
  swaggerDefinition: {
    info: {
      title: packageJson.name,
      version: packageJson.version,
      description: packageJson.description,
    },
  },
  apis: ['src/**/actions/*.js'],
});
