const { HttpError } = require('../errors/http.error');
const { AppError } = require('../errors/app.error');
const httpStatus = require('http-status-codes');
const { logger } = require('../tools/logger');
const { isCelebrate } = require('celebrate');

const errorHandler = (err, req, res, next) => {
  logger.error(err.toString());

  if (isCelebrate(err)) {
    return res.status(httpStatus.BAD_REQUEST).json(err);
  }

  if (err instanceof HttpError) {
    return res.status(err.status).json({
      message: err.message,
    });
  }

  if (err instanceof AppError) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
      message: err.message,
    });
  }

  return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
    message: 'Something went wrong',
  });
};

module.exports = { errorHandler };
