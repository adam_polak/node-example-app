class TypeormPostsRepository {
  constructor() {
    this.posts = [];
  }

  find(options) {
    return Promise.resolve(this.posts.slice(options.skip, options.take));
  }

  save(post) {
    this.posts.push(post);

    return Promise.resolve(post);
  }
}

module.exports = { TypeormPostsRepository };
