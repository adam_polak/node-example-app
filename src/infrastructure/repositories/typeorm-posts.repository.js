class TypeormPostsRepository {
  constructor(connection) {
    this.connection = connection;
  }

  find(options) {
    return this.connection.getRepository('Post').find(options);
  }

  async save(post) {
    return this.connection.getRepository('Post').save(post);
  }
}

module.exports = { TypeormPostsRepository };
