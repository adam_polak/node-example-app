const { EntitySchema } = require('typeorm');

module.exports = new EntitySchema({
  name: 'Post',
  columns: {
    id: {
      primary: true,
      type: 'uuid',
    },
    title: {
      type: 'varchar',
    },
    content: {
      type: 'varchar',
    },
  },
});
