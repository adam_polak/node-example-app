require('dotenv-safe').config();

const { logger } = require('./tools/logger');
const { server } = require('./app/server');
const { createCommandBus } = require('./app/command-bus');
const { createConnection } = require('typeorm');
const { TypeormPostsRepository } = require('./infrastructure/repositories/typeorm-posts.repository');

process.on('unhandledRejection', err => {
  logger.error(err.toString());
  process.exit(1);
});

process.on('SIGINT', () => {
  logger.error('sigint error');
  process.exit(2);
});

process.on('SIGTERM', () => {
  logger.error('sigterm error');
  process.exit(3);
});

createConnection().then(connection => {
  const app = server({
    commandBus: createCommandBus({
      postsRepository: new TypeormPostsRepository(connection),
    }),
  });

  app
    .listen(process.env.PORT, () => {
      logger.info(`Server executed on port ${process.env.PORT}`);
    })
    .on('error', err => {
      logger.error(err.toString());
    });
});
