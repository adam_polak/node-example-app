const { CommandBus } = require('./command-bus');

describe('Command bus', () => {
  it('returns an error when invalid handler without supports method passed', () => {
    const commandBus = new CommandBus();

    expect(() => commandBus.addHandler({})).toThrow(
      'Invalid handler instance given. One of methods is missing: "supports" or "handle"'
    );
  });

  it('returns an error when invalid handler without handle method passed', () => {
    const commandBus = new CommandBus();

    expect(() =>
      commandBus.addHandler({
        supports: jest.fn(),
      })
    ).toThrow('Invalid handler instance given. One of methods is missing: "supports" or "handle"');
  });

  it('returns an error when no handler found for specific command', () => {
    const commandBus = new CommandBus();

    commandBus.addHandler({
      supports: () => false,
      handle: command => command,
    });

    expect(() =>
      commandBus.handle({
        type: 'MY_COMMAND',
      })
    ).toThrow('Could not find handler for command with type: MY_COMMAND');
  });

  it('handles a command', () => {
    const commandBus = new CommandBus();

    commandBus.addHandler({
      supports: command => command.type === 'SUPPORTED_COMMAND',
      handle: command => command,
    });

    expect(
      commandBus.handle({
        type: 'SUPPORTED_COMMAND',
      })
    ).toEqual({
      type: 'SUPPORTED_COMMAND',
    });
  });
});
