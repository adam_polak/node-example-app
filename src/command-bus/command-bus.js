const { AppError } = require('../errors/app.error');

class CommandBus {
  constructor() {
    this.handlers = [];
  }

  addHandler(handler) {
    if (typeof handler.supports !== 'function' || typeof handler.handle !== 'function') {
      throw new AppError(`Invalid handler instance given. One of methods is missing: "supports" or "handle"`);
    }

    this.handlers.push(handler);
  }

  async handle(command) {
    const handler = this.handlers.find(handler => handler.supports(command));

    if (!handler) {
      throw new AppError(`Could not find handler for command with type: ${command.type}`);
    }

    return handler.handle(command);
  }
}

module.exports = { CommandBus };
