const { CommandBus } = require('../command-bus/command-bus');
const { GetPostsHandler } = require('../routes/posts/handlers/get-posts.handler');
const { CreatePostsHandler } = require('../routes/posts/handlers/create-posts.handler');

const createCommandBus = dependencies => {
  const commandBus = new CommandBus();

  commandBus.addHandler(new GetPostsHandler(dependencies.postsRepository));
  commandBus.addHandler(new CreatePostsHandler(dependencies.postsRepository));

  return commandBus;
};

module.exports = { createCommandBus };
