const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const swaggerUi = require('swagger-ui-express');
const jsdoc = require('../tools/swagger');
const { NotFoundError } = require('../errors/not-found.error');
const { createRoutes } = require('../routes');
const { errorHandler } = require('../middleware/error-handler');

const server = dependencies => {
  const app = express();

  app.use(cors());
  app.use(helmet());
  app.use(express.json());
  app.use(morgan(process.env.NODE_ENV === 'development' ? 'dev' : 'combined'));

  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(jsdoc));
  app.use('/api', createRoutes(dependencies.commandBus));

  app.use('*', (req, res, next) => next(new NotFoundError('Page not found')));
  app.use(errorHandler);

  return app;
};

module.exports = { server };
