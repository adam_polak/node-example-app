const TYPE = 'GET_POSTS';

class GetPostsCommand {
  constructor(limit = 10, offset = 0) {
    this.type = TYPE;
    this.limit = limit;
    this.offset = offset;
  }
}

module.exports = { GetPostsCommand, TYPE };
