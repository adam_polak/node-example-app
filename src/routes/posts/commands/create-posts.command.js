const TYPE = 'CREATE_POSTS';

class CreatePostsCommand {
  constructor(id, title, content) {
    this.id = id;
    this.title = title;
    this.content = content;
    this.type = TYPE;
  }
}

module.exports = { CreatePostsCommand, TYPE };
