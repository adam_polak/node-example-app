const { TYPE } = require('../commands/get-posts.command');

class GetPostsHandler {
  constructor(postsRepotitory) {
    this.postsRepotitory = postsRepotitory;
  }

  supports(command) {
    return command.type === TYPE;
  }

  handle(command) {
    return this.postsRepotitory.find({
      take: command.limit,
      skip: command.offset,
    });
  }
}

module.exports = { GetPostsHandler };
