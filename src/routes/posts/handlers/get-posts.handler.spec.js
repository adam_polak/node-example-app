jest.mock('../../../infrastructure/repositories/typeorm-posts.repository.js');

const { TypeormPostsRepository } = require('../../../infrastructure/repositories/typeorm-posts.repository.js');
const { GetPostsHandler } = require('./get-posts.handler');
const { GetPostsCommand } = require('../commands/get-posts.command');

describe('Get posts handler', () => {
  it('should call posts repository with given parameters', () => {
    const getPostsHandler = new GetPostsHandler(new TypeormPostsRepository());
    const command = new GetPostsCommand();

    return getPostsHandler.handle(command).then(posts => {
      expect(posts).toEqual([]);
    });
  });
});
