const { TYPE } = require('../commands/create-posts.command');

class CreatePostsHandler {
  constructor(postsRepotitory) {
    this.postsRepotitory = postsRepotitory;
  }

  supports(command) {
    return command.type === TYPE;
  }

  handle(command) {
    return this.postsRepotitory.save({
      id: command.id,
      title: command.title,
      content: command.content,
    });
  }
}

module.exports = { CreatePostsHandler };
