jest.mock('uuid', () => ({
  v4: () => '81d18e4a-45dd-4bcf-b79b-2abd8b932663',
}));

const { server } = require('../../../app/server');
const { createCommandBus } = require('../../../app/command-bus');
const { TypeormPostsRepository } = require('../../../infrastructure/repositories/typeorm-posts.repository');
const { createConnection } = require('typeorm');
const request = require('supertest');

describe('Create posts integration tests', () => {
  let connection;
  let app;

  beforeAll(async () => {
    connection = await createConnection();
    app = server({
      commandBus: createCommandBus({
        postsRepository: new TypeormPostsRepository(connection),
      }),
    });
  });

  afterAll(async () => {
    return connection.close();
  });

  describe('Validations', () => {
    it('validates posts request', () => {
      return request(app)
        .post('/api/posts')
        .send({})
        .expect(400)
        .catch(err => {
          console.log(err);
          throw err;
        });
    });
  });

  describe('Creation', () => {
    beforeEach(async () => {
      await connection.dropDatabase();
      await connection.synchronize();
    });

    it('creates post', () => {
      return request(app)
        .post('/api/posts')
        .send({
          title: 'my-title',
          content: 'some content',
        })
        .expect(201)
        .then(response => {
          expect(response.body).toEqual({
            id: '81d18e4a-45dd-4bcf-b79b-2abd8b932663',
            title: 'my-title',
            content: 'some content',
          });
        });
    });

    it('creates second posts', () => {
      return request(app)
        .post('/api/posts')
        .send({
          title: 'my-title',
          content: 'some content',
        })
        .expect(201)
        .then(response => {
          expect(response.body).toEqual({
            id: '81d18e4a-45dd-4bcf-b79b-2abd8b932663',
            title: 'my-title',
            content: 'some content',
          });
        });
    });
  });
});
