const express = require('express');
const { getPosts } = require('./actions/get-posts.action');
const { createPosts } = require('./actions/create-posts.action');
const { celebrate, Joi } = require('celebrate');

const postsRouting = commandBus => {
  const router = express.Router();

  router.get('/', getPosts(commandBus));
  router.post(
    '/',
    [
      celebrate(
        {
          body: Joi.object().keys({
            title: Joi.string().required(),
            content: Joi.string().required(),
          }),
        },
        { abortEarly: false }
      ),
    ],
    createPosts(commandBus)
  );

  return router;
};

module.exports = { postsRouting };
