const { CreatePostsCommand } = require('../commands/create-posts.command');
const httpStatusCodes = require('http-status-codes');
const { v4 } = require('uuid');

/**
 * @swagger
 *
 * /api/posts:
 *   post:
 *     description: Creates a new post
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: payload
 *         description: Create post contract
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             title:
 *               type: string
 *             content:
 *               type: string
 *           required:
 *             - title
 *             - content
 *     responses:
 *       201:
 *         description: Creates a new post
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Internal Server Error
 */
const createPosts = commandBus => (req, res, next) => {
  commandBus
    .handle(new CreatePostsCommand(v4(), req.body.title, req.body.content))
    .then(post => {
      res.status(httpStatusCodes.CREATED);
      res.json({
        ...post,
      });
    })
    .catch(next);
};

module.exports = { createPosts };
