const { GetPostsCommand } = require('../commands/get-posts.command');
const httpStatusCodes = require('http-status-codes');

/**
 * @swagger
 *
 * /api/posts:
 *   get:
 *     description: Return list of posts
 *     responses:
 *       200:
 *         description: Return list of posts
 *       400:
 *         description: Validation Error
 *       500:
 *         description: Internal Server Error
 */
const getPosts = commandBus => (req, res, next) => {
  commandBus
    .handle(new GetPostsCommand())
    .then(posts => {
      res.status(httpStatusCodes.OK);
      res.json({
        data: posts,
      });
    })
    .catch(next);
};

module.exports = { getPosts };
