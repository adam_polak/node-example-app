jest.mock('../../../infrastructure/repositories/typeorm-posts.repository.js');

const { TypeormPostsRepository } = require('../../../infrastructure/repositories/typeorm-posts.repository.js');
const { GetPostsHandler } = require('../handlers/get-posts.handler');
const { CommandBus } = require('../../../command-bus/command-bus');
const { getPosts } = require('./get-posts.action');

describe('Get posts action', () => {
  it('should return posts', () => {
    const commandBus = new CommandBus();
    commandBus.addHandler(new GetPostsHandler(new TypeormPostsRepository()));

    const getPostsAction = getPosts(commandBus);
    const next = jest.fn();
    const req = jest.fn();
    const res = {
      status: jest.fn(),
      json: jest.fn(),
    };

    getPostsAction(req, res, next);

    setImmediate(() => {
      expect(next).not.toHaveBeenCalled();
      expect(res.status).toHaveBeenCalledWith(200);
      expect(res.json).toHaveBeenCalledWith({
        data: [],
      });
    });
  });
});
