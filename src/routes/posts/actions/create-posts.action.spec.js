jest.mock('../../../infrastructure/repositories/typeorm-posts.repository.js');
jest.mock('uuid', () => ({
  v4: () => '81d18e4a-45dd-4bcf-b79b-2abd8b932663',
}));

const { TypeormPostsRepository } = require('../../../infrastructure/repositories/typeorm-posts.repository.js');
const { CreatePostsHandler } = require('../handlers/create-posts.handler');
const { CommandBus } = require('../../../command-bus/command-bus');
const { createPosts } = require('./create-posts.action');

describe('Create posts handler', () => {
  it('should create new post', () => {
    const commandBus = new CommandBus();
    commandBus.addHandler(new CreatePostsHandler(new TypeormPostsRepository()));

    const createPostsAction = createPosts(commandBus);
    const next = jest.fn();
    const req = {
      body: {
        title: 'fake-title',
        content: 'fake-content',
      },
    };
    const res = {
      status: jest.fn(),
      json: jest.fn(),
    };

    createPostsAction(req, res, next);

    setImmediate(() => {
      expect(next).not.toHaveBeenCalled();
      expect(res.status).toHaveBeenCalledWith(201);
      expect(res.json).toHaveBeenCalledWith({
        id: '81d18e4a-45dd-4bcf-b79b-2abd8b932663',
        title: 'fake-title',
        content: 'fake-content',
      });
    });
  });
});
