const express = require('express');
const { postsRouting } = require('./posts/posts.routing');

const createRoutes = commandBus => {
  const router = express.Router();

  router.use('/posts', postsRouting(commandBus));

  return router;
};

module.exports = { createRoutes };
